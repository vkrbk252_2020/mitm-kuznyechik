#ifndef DIFFERENCIALWAY_H
#define DIFFERENCIALWAY_H

#include <stdint.h> 
#include <vector>
#include "Kuznechik.h"

typedef struct Tuple {
    uint32_t X2;
    uint32_t X3;
    uint8_t X4_0;

    Tuple(uint32_t x2, uint32_t x3, uint8_t x4_0) : X2(x2), X3(x3), X4_0(x4_0) {}
} Tuple;

class DifferencialWay {

    private:

    public:
    DifferencialWay(Kuznechik &kuz);

    vector<vector<vector<bool>>> sBoxEquationResultsTable;

    void precomputesBoxEquationResults(Kuznechik &kuz);
    
    void directDifferential(Kuznechik &kuznechik, vector<uint32_t> &dX3Results, vector<uint32_t> &X2Results);
    void reverseDifferential(Kuznechik &kuznechik, vector<uint32_t> &dY3Results, vector<uint8_t> &X4_0Results);

    void checkMiddleValues( Kuznechik &kuznechik, 
                            vector<uint32_t> &dX3Results,  
                            vector<uint32_t> &dY3Results, 
                            vector<uint32_t> &X2Results,
                            vector<uint8_t> &X4_0Results,
                            vector<Tuple> & x2x3x4results);
};

#endif //DIFFERENCIALWAY_H