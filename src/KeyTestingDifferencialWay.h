#ifndef KEYTESTINGDIFFERENCIALWAY_H
#define KEYTESTINGDIFFERENCIALWAY_H
#include "Kuznechik.h"
#include "MultidimensionalDifferential.h"
#include <unordered_map>

class KeyTestingDifferencialWay {


public:

    KeyTestingDifferencialWay(Kuznechik &kuznechik);

    vector<vector<vector<bool>>> sBoxEquationResultsTable;

    void precomputesBoxEquationResults(Kuznechik &kuznechik);

    void findKey(Kuznechik &kuznechik, unordered_set<uint32_t> &dY4_0Msets,unordered_map<uint32_t, int> &K6);
    
};

#endif //KEYTESTINGDIFFERENCIALWAY_H