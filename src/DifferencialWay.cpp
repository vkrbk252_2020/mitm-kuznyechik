#include "DifferencialWay.h"

using namespace std;

DifferencialWay::DifferencialWay(Kuznechik &kuz){
    precomputesBoxEquationResults(kuz);
}

void DifferencialWay::precomputesBoxEquationResults(Kuznechik &kuz) {
    sBoxEquationResultsTable = vector<vector<vector<bool>>>(16);

    for (size_t i = 0; i < 16; i++)
    {
        sBoxEquationResultsTable[i] = vector<vector<bool>>(16);
        for (size_t j = 0; j < 16; j++)
        {
            sBoxEquationResultsTable[i][j] = vector<bool>(16);
        }
        
    }
    

    for (size_t dXi= 0; dXi < 16; dXi++)
    {
        for (size_t dYi = 0; dYi < 16; dYi++)
        {
            for (size_t t = 0; t < 16; t++)
            {
                sBoxEquationResultsTable[dXi][dYi][t] = ((kuz.sbox[dXi ^ t] ^ kuz.sbox[t]) == dYi);        
            }       
        }
    }
}

void DifferencialWay::directDifferential(Kuznechik &kuznechik, vector<uint32_t> &dX3Results, vector<uint32_t> &X2Results) {   
    for (size_t i = 1; i < 16; i++) {
        for (size_t j = 0; j <= 0xFFFFFF; j++) {
            uint32_t dY1 = i;
            uint32_t X2 = j;

            uint32_t dX2 = dY1;
            kuznechik.linearTransform(dX2);

            uint32_t X2Prime = X2 ^ dX2;

            uint32_t Z2 = X2;
            Z2 = kuznechik.LS[Z2];

            uint32_t Z2Prime = X2Prime;
            Z2Prime = kuznechik.LS[Z2Prime];

            uint32_t dX3 = Z2 ^ Z2Prime;
            dX3Results.push_back(dX3);
            X2Results.push_back(X2);
       }
    }
}

void DifferencialWay::reverseDifferential(Kuznechik &kuznechik, vector<uint32_t> &dY3Results, vector<uint8_t> &X4_0Results) {
    for (size_t i = 0; i < 16; i++) {
        for (size_t j = 1; j < 16; j++) {
            uint8_t Y4_0 = i;
            uint8_t dY4_0 = j;

            uint8_t Y4Prime_0 = Y4_0 ^ dY4_0;

            uint8_t X4_0 = kuznechik.sboxInv[Y4_0];

            uint8_t X4Prime_0 = kuznechik.sboxInv[Y4Prime_0];

            uint32_t dZ3 = X4_0 ^ X4Prime_0;
            uint32_t dY3 = dZ3;
            
            kuznechik.linearTransformInv(dY3);

            dY3Results.push_back(dY3);
            X4_0Results.push_back(X4_0);
        }
    }
}

void DifferencialWay::checkMiddleValues(Kuznechik &kuznechik, 
                                        vector<uint32_t> &dX3Results, 
                                        vector<uint32_t> &dY3Results,
                                        vector<uint32_t> &X2Results,
                                        vector<uint8_t> &X4_0Results,
                                        vector<Tuple> &x2x3x4results) {

    for (size_t i = 0; i < dX3Results.size(); i++) {
        for (size_t j = 0; j < dY3Results.size(); j++) {
            
            vector<vector<uint8_t>> X2BlocksTable = vector<vector<uint8_t>>();
            for (size_t k = 0; k < kuznechik.BLOCK_SIZE; k+=4) {

                uint8_t dXi = dX3Results[i] >> k;
                dXi&=kuznechik.mask4bits;

                uint8_t dYi = dY3Results[j] >> k;
                dYi&=kuznechik.mask4bits;

                vector<uint8_t> bits = vector<uint8_t>();

                for (uint8_t t = 0; t < 16; t++) {
                    if (sBoxEquationResultsTable[dXi][dYi][t]) {
                        bits.push_back(t);
                    }
                }
                //если не нашлось ни одного t = 0..15, которое удовлетворяет "маленькому" уравнению
                if (bits.empty()){
                    break;
                }
                X2BlocksTable.push_back(bits);
            }
            //если к хотя бы одному "маленькому" уравнению не нашелся ответ
            if (X2BlocksTable.size() < 6) {
                break;
            }

            for (size_t a = 0; a < X2BlocksTable[0].size(); a++){
                for (size_t b = 0; b < X2BlocksTable[1].size(); b++){
                    for (size_t c = 0; c < X2BlocksTable[2].size(); c++){
                        for (size_t d = 0; d < X2BlocksTable[3].size(); d++){
                            for (size_t e = 0; e < X2BlocksTable[4].size(); e++){
                                for (size_t f = 0; f < X2BlocksTable[5].size(); f++){
                                        uint32_t X3 =
                                                    X2BlocksTable[0][a] << 0 | X2BlocksTable[1][b] << 4 |
                                                    X2BlocksTable[2][c] << 8 | X2BlocksTable[3][d] << 12 |
                                                    X2BlocksTable[4][e] << 16 | X2BlocksTable[5][f] << 20;

                                        x2x3x4results.push_back(Tuple(X2Results[i], X3, X4_0Results[j]));
                                }
                            }
                        }
                    }
                }

            }
        }
    }
}