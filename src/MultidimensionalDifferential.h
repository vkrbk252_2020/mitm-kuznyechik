#ifndef MULTIDIMENSIONALDIFFERENTIAL_H
#define MULTIDIMENSIONALDIFFERENTIAL_H

#include <stdint.h> 
#include <vector>
#include "Kuznechik.h"
#include "DifferencialWay.h"
#include "Mset.h"
#include <unordered_set>

class MultidimensionalDifferential {
    private:
  
    public:
    
    MultidimensionalDifferential();

    vector<uint32_t> dY1;

    unordered_set<uint32_t> computeDy4_0MSets(Kuznechik &kuznechik, vector<Tuple> &x2x3x4results);

};

#endif //MULTIDIMENSIONALDIFFERENTIAL_H