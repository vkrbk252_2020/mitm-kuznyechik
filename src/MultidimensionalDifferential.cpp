#include "MultidimensionalDifferential.h"

using namespace std;

MultidimensionalDifferential::MultidimensionalDifferential() {
    dY1 = vector<uint32_t>(15);
    for (uint32_t i = 0; i < 15; i++) {
        dY1[i] = i + 1;
    }
}

unordered_set<uint32_t> MultidimensionalDifferential::computeDy4_0MSets(
    Kuznechik &kuznechik, 
    vector<Tuple> &x2x3x4results
    ){
        unordered_set<uint32_t> dY4_0Msets = unordered_set<uint32_t>();
        
        for (size_t i = 0; i < x2x3x4results.size(); i++) {

            //Ищем вектор dY2
            vector<uint32_t> dY2 = vector<uint32_t>(15);
            vector<uint32_t> dX2 = dY1;
            for (size_t j = 0; j < 15; j++) {
                kuznechik.linearTransform(dX2[j]);
                dX2[j] ^= x2x3x4results[i].X2;
                kuznechik.nonLinearTransform(dX2[j]);
                uint32_t sBoxX2 = x2x3x4results[i].X2;
                kuznechik.nonLinearTransform(sBoxX2);
                dX2[j]^=sBoxX2;
            }
            dY2 = dX2;
            
            //Ищем вектор dY3
            vector<uint32_t> dY3 = vector<uint32_t>(15);
            vector<uint32_t> dX3 = dY2;
            for (size_t j = 0; j < 15; j++) {
                kuznechik.linearTransform(dX3[j]);
                dX3[j] ^= x2x3x4results[i].X3;
                kuznechik.nonLinearTransform(dX3[j]);
                uint32_t sBoxX3 = x2x3x4results[i].X3;
                kuznechik.nonLinearTransform(sBoxX3);
                dX3[j]^=sBoxX3;
            }
            dY3 = dX3;

            //Ищем вектор dY4[0]
            vector<uint8_t> dY4_0 = vector<uint8_t>(15);
                vector<uint32_t> dX4 = dY3;
                for (size_t j = 0; j < 15; j++) {
                    kuznechik.linearTransform(dX4[j]);
                    dY4_0[j] = (uint8_t)dX4[j];
                    dY4_0[j]&=0x0F;
                    dY4_0[j] = kuznechik.sbox[dY4_0[j] ^ x2x3x4results[i].X4_0];
                    uint8_t sBoxX4_0 = x2x3x4results[i].X4_0;
                    sBoxX4_0 = kuznechik.sbox[sBoxX4_0];
                    dY4_0[j]^=sBoxX4_0;
                }                
            
            // Делаем mset(dY4_0)
            uint64_t dY4_0Data = 0;
            for (size_t j = 0; j < 15; j++) {
                uint64_t dY4_0_64 = dY4_0[j];
                dY4_0_64 =dY4_0_64<<(j*4);
                dY4_0Data |= dY4_0_64;
            }
            uint32_t mset = Mset::makePair(dY4_0Data);
            dY4_0Msets.insert(mset);

            
    }   
    return dY4_0Msets;
}