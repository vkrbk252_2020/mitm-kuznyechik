#ifndef MSET_H
#define MSET_H

#include <stdint.h> 

class Mset {
   
public:

    static uint32_t makePair(const uint64_t &data) {
        uint16_t nums = 0;
        uint16_t divides = 0;

        uint8_t halfBytes[16] = {0};

        for (size_t i = 0; i < 64; i+=4) {
            uint8_t halfByte = data >> i;
            halfByte&=0x0F;

            halfBytes[halfByte]++;
        }

        uint8_t dividesShift = 0;
        for (size_t i = 0; i < 16; i++) {
            if (halfBytes[i] > 0) {
                nums = nums | (0x01 << i);
                dividesShift+=halfBytes[i];
                divides = divides | (0x01 << dividesShift);
            }
        }
        uint32_t result = 0;
        result |= nums;
        result = result<<16;
        result |= divides;


        return result;
    }

    static uint64_t makeData(const uint32_t &pair){
        uint64_t data = 0;
        uint8_t prev = 0;
        uint8_t next = 0;
        
        uint16_t divides = (uint16_t) pair;
        uint16_t nums = pair>>16;

        for (uint8_t i = 0; i < 16; i++) {
            if (nums & (0x01 << i)) {
               for (; next < 16; next++) {
                    if (divides & (0x01 << next)) {
                        break;
                    }
               }
               for (int k = 0; k < next - prev; k++) {
                    data = (data << 4) | (uint64_t)(i & 0x0F);
                }
                prev = next;
                next++;
            }  
        }
        return data;
    }
};


#endif //MSET_H