#include "Kuznechik.h"
#include <iostream>

/*----------------------------------------Constructors & Destructor----------------------------------------*/

using namespace std;

Kuznechik::Kuznechik() {
    BLOCK_SIZE = 24;
    ROUNDS_NUMBER = 5;
    LS_ARRAY_SIZE = 0xFFFFFF + 1;

    CONSTANTS_ARRAY_SIZE = (ROUNDS_NUMBER / 2 + ROUNDS_NUMBER % 2) * 8;
    ROUND_KEY_ARRAY_SIZE = (ROUNDS_NUMBER / 2 + ROUNDS_NUMBER % 2) * 2;

    blockSizeMask = 0x00ffffff;

    uint64_t key = 0x1345afcc569a;

    LS = vector<uint32_t>(LS_ARRAY_SIZE);
    for (size_t i = 0; i < LS_ARRAY_SIZE; i++) {
        uint32_t temp = i;
        nonLinearTransform(temp);
        linearTransform(temp);
        LS[i] = temp;
    }   

    LSInv = vector<uint32_t>(LS_ARRAY_SIZE);
    for (size_t i = 0; i < LS_ARRAY_SIZE; i++) {
        uint32_t temp = i;
        linearTransformInv(temp);
        nonLinearTransformInv(temp);
        LSInv[i] = temp;
    }   

    setConstants();
    expandKey(key);
    
}

// При вызове этого конструктора не забыть поменять матрицы L8/L7/L6/L5/L4
Kuznechik::Kuznechik(uint64_t initKey, size_t initBlockSize, size_t initRoundsNumber, size_t initLSArraySize, uint32_t initBlockSizeMask) {

    BLOCK_SIZE = initBlockSize;
    ROUNDS_NUMBER = initRoundsNumber;
    LS_ARRAY_SIZE = initLSArraySize;
    blockSizeMask = initBlockSizeMask;


    CONSTANTS_ARRAY_SIZE = (ROUNDS_NUMBER / 2 + ROUNDS_NUMBER % 2) * 8;
    ROUND_KEY_ARRAY_SIZE = (ROUNDS_NUMBER / 2 + ROUNDS_NUMBER % 2) * 2;

    uint64_t key = initKey;

    LS = vector<uint32_t>(LS_ARRAY_SIZE);
    for (size_t i = 0; i < LS_ARRAY_SIZE; i++) {
        uint32_t temp = i;
        nonLinearTransform(temp);
        linearTransform(temp);
        LS[i] = temp;
    }   

    LSInv = vector<uint32_t>(LS_ARRAY_SIZE);
    for (size_t i = 0; i < LS_ARRAY_SIZE; i++) {
        uint32_t temp = i;
        linearTransformInv(temp);
        nonLinearTransformInv(temp);
        LSInv[i] = temp;
    }   

    setConstants();
    expandKey(key);
    
}

/*----------------------------------------Key expansion----------------------------------------*/

void Kuznechik::setConstants(){

	constants = vector<uint32_t>(CONSTANTS_ARRAY_SIZE);
	for (size_t i = 0; i < (CONSTANTS_ARRAY_SIZE); i++) {
		constants[i] = i + 1;
        linearTransform(constants[i]);
        constants[i]&=blockSizeMask;
	}
}


void Kuznechik::expandKey(uint64_t& key) {

	roundKeys = vector<uint32_t>(ROUND_KEY_ARRAY_SIZE);
    
    for(size_t i = 0; i < 2; i += 1) {
        roundKeys[i] = (key >> i * BLOCK_SIZE);
        roundKeys[i]&= blockSizeMask;
    }

    for (size_t i = 2, k=0; i < roundKeys.size(); i+=2 ,k+=8) {

        uint32_t temp1 = roundKeys[i-2];
        uint32_t temp2 = roundKeys[i-1];

        for (size_t j = k; j < k+8; j++) {
           keyExpansionRoundFunction(j, temp1, temp2);
        }
        roundKeys[i] = temp1;
        roundKeys[i] &= blockSizeMask;
        roundKeys[i+1] = temp2;
        roundKeys[i+1]&= blockSizeMask;
    }    
}

void Kuznechik::keyExpansionRoundFunction(size_t roundNumber, uint32_t& roundKey1, uint32_t& roundKey2) {

    roundKey2 = roundKey1;

    xorBlocks(roundKey1, constants[roundNumber]);
    roundKey1 = LS[roundKey1];

    xorBlocks(roundKey1, roundKey2);    

}


void Kuznechik::xorBlocks(uint32_t& block1, uint32_t& block2) {
        block1 ^= block2;
}

/*----------------------------------------Encryption----------------------------------------*/



void Kuznechik::addRoundKey(uint32_t& block, uint32_t& roundKey) {
     block ^= roundKey;
}

void Kuznechik::nonLinearTransform(uint32_t& block) {

    for (size_t i = 0; i < BLOCK_SIZE; i+=4) {

        uint8_t little4Bits = (uint8_t)(block >> i);
        little4Bits&=mask4bits;

        little4Bits = sbox[little4Bits];

        uint32_t sboxResultBits = little4Bits;
        sboxResultBits = sboxResultBits << i;

        //обнуляем 4 бита входного блока на правильной позиции для замены их на их sbox
        uint32_t blockMask = 0xf;
        blockMask = blockMask << i;
        block&=~blockMask;

        //устанавливаем sbox 4-х бит на их место в блоке
        block^=sboxResultBits;  

    }
}


void Kuznechik::linearTransform(uint32_t& block) {

    uint32_t result = 0x0;

	for (size_t i = 0; i < BLOCK_SIZE; i+=4) {
        uint8_t result8Bits = 0x0;

		for (size_t j = 0; j < BLOCK_SIZE; j+=4) {

            uint8_t little4Bits = (uint8_t)(block >> j);
            little4Bits&=mask4bits;
            result8Bits ^= MulMatrix [little4Bits] [L6[j/4][i/4]];            
		}

        //устанавливаем sbox 4-х бит на их место в блоке
        uint32_t linearResultBits = result8Bits;
        linearResultBits = linearResultBits << i;
        result^=linearResultBits;  
        
	}

    block = result;
}

void Kuznechik::encryptRoundFunction(uint32_t& block, uint32_t roundNumber){
    addRoundKey(block, roundKeys[roundNumber]);
    block = LS[block];
}

void Kuznechik::encryptBlock(uint32_t& input, uint32_t& output) {
    output = input;
    for (size_t round = 0; round < ROUNDS_NUMBER; round++) {
         encryptRoundFunction(output, round);
    }
    addRoundKey(output, roundKeys[ROUNDS_NUMBER]);

}

void Kuznechik::encryptBlockWithEquivalentLastRoundPresentation(uint32_t& input, uint32_t& output){
     output = input;
     for (size_t round = 0; round < ROUNDS_NUMBER - 2; round++) {
         encryptRoundFunction(output, round);
    }
    addRoundKey(output, roundKeys[ROUNDS_NUMBER - 2]);
    nonLinearTransform(output);
    
    uint32_t lastRoundKey = roundKeys[ROUNDS_NUMBER-1];
    linearTransformInv(lastRoundKey);
    lastKey = lastRoundKey;
    addRoundKey(output, lastRoundKey);
}

/*----------------------------------------Decryption----------------------------------------*/

void Kuznechik::nonLinearTransformInv(uint32_t& block) {

    for (size_t i = 0; i < BLOCK_SIZE; i+=4) {

        uint8_t little4Bits = (uint8_t)(block >> i);
        little4Bits&=mask4bits;

        little4Bits = sboxInv[little4Bits];

        uint32_t sboxResultBits = little4Bits;
        sboxResultBits = sboxResultBits << i;

        //обнуляем 4 бита входного блока на правильной позиции для замены их на их sbox
        uint32_t blockMask = 0xf;
        blockMask = blockMask << i;
        block&=~blockMask;

        //устанавливаем sbox 4-х бит на их место в блоке
        block^=sboxResultBits;  

    }
}

void Kuznechik::linearTransformInv(uint32_t& block) {

    uint32_t result = 0x0;

	for (size_t i = 0; i < BLOCK_SIZE; i+=4) {
        uint8_t result8Bits = 0x0;

		for (size_t j = 0; j < BLOCK_SIZE; j+=4) {

            uint8_t little4Bits = (uint8_t)(block >> j);
            little4Bits&=mask4bits;
            result8Bits ^= MulMatrix [little4Bits] [L6Inv[j/4][i/4]];            
		}

        //устанавливаем sbox 4-х бит на их место в блоке
        uint32_t linearResultBits = result8Bits;
        linearResultBits = linearResultBits << i;
        result^=linearResultBits;  
        
	}

    block = result;

}

void Kuznechik::decryptRoundFunction(uint32_t& block, uint32_t roundNumber) {
    block = LSInv[block];
    addRoundKey(block, roundKeys[roundNumber]);
}

void Kuznechik::decryptBlock(uint32_t& input, uint32_t& output) {
    output = input;

    addRoundKey(output, roundKeys[ROUNDS_NUMBER]);

    for (int round = ROUNDS_NUMBER - 1; round >= 0; round--) {
        decryptRoundFunction(output, round);
    }
    
}