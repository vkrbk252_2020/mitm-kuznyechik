#include "KeyTestingDifferencialWay.h"
#include <algorithm>
#include "Mset.h"

using namespace std;

KeyTestingDifferencialWay::KeyTestingDifferencialWay(Kuznechik &kuznechik){
    precomputesBoxEquationResults(kuznechik);
}

void KeyTestingDifferencialWay::precomputesBoxEquationResults(Kuznechik &kuznechik) {
    sBoxEquationResultsTable = vector<vector<vector<bool>>>(16);

    for (size_t i = 0; i < 16; i++) {
        sBoxEquationResultsTable[i] = vector<vector<bool>>(16);
        for (size_t j = 0; j < 16; j++) {
            sBoxEquationResultsTable[i][j] = vector<bool>(16);
        }
        
    }
    

    for (size_t dXi= 0; dXi < 16; dXi++) {
        for (size_t dYi = 0; dYi < 16; dYi++) {
            for (size_t t = 0; t < 16; t++) {
                sBoxEquationResultsTable[dXi][dYi][t] = ((kuznechik.sbox[dXi ^ t] ^ kuznechik.sbox[t]) == dYi);        
            }       
        }
    }
}

    void KeyTestingDifferencialWay::findKey(Kuznechik &kuznechik, unordered_set<uint32_t> &dY4_0Msets,unordered_map<uint32_t, int> &K6) {

        for (uint32_t i = 0; i <= 0xFFFFFF; i+=16) {
            vector<uint32_t> PResults = vector<uint32_t>();
            uint32_t P = i;
            PResults.push_back(P);
            for (size_t j = 1; j < 16; j++) {
                PResults.push_back(P+j);
            }

            vector<uint32_t> CResults = vector<uint32_t>(16);
            for (size_t j = 0; j < 16; j++) {
                CResults[j] = PResults[j];
                kuznechik.encryptBlockWithEquivalentLastRoundPresentation(PResults[j], CResults[j]);
            }

            vector<pair<uint32_t,uint32_t>> dCResultsWithAnchor = vector<pair<uint32_t,uint32_t>>();

            for (size_t j = 0; j < 16; j++){
                for (size_t k = 0; k < 16; k++) {
                    if(j < k) {
                        dCResultsWithAnchor.push_back(pair(CResults[j]^CResults[k], CResults[j]));
                    }
                }
            }

            vector<uint32_t> dY5 = vector<uint32_t>(120);

            for(size_t index = 0; index < dCResultsWithAnchor.size(); index++){
                dY5[index] = dCResultsWithAnchor[index].first;
            }

            vector<uint32_t> dX5 = vector<uint32_t>(15);
            for (uint32_t j = 1; j < 16; j++) {
                dX5[j] = j;
                kuznechik.linearTransform(dX5[j]);
            }

            for (uint32_t x = 0; x < dX5.size(); x++) {
                for (uint32_t y = 0; y < dY5.size(); y++) {

                    vector<vector<uint8_t>> X5BlocksTable = vector<vector<uint8_t>>();
                    for (size_t k = 0; k < kuznechik.BLOCK_SIZE; k+=4) {

                        uint8_t dXi = dX5[x] >> k;
                        dXi&=kuznechik.mask4bits;

                        uint8_t dYi = dY5[y] >> k;
                        dYi&=kuznechik.mask4bits;

                        vector<uint8_t> bits = vector<uint8_t>();

                        for (uint8_t t = 0; t < 16; t++) {
                            if (sBoxEquationResultsTable[dXi][dYi][t]) {
                                bits.push_back(t);
                            }
                        }
                        //если не нашлось ни одного t = 0..15, которое удовлетворяет "маленькому" уравнению
                        if (bits.empty()){
                            break;
                        }
                        X5BlocksTable.push_back(bits);
                    }
                    //если к хотя бы одному "маленькому" уравнению не нашелся ответ
                    if (X5BlocksTable.size() < 6) {
                        break;
                    }

                    for (size_t a = 0; a < X5BlocksTable[0].size(); a++){
                        for (size_t b = 0; b < X5BlocksTable[1].size(); b++){
                            for (size_t c = 0; c < X5BlocksTable[2].size(); c++){
                                for (size_t d = 0; d < X5BlocksTable[3].size(); d++){
                                    for (size_t e = 0; e < X5BlocksTable[4].size(); e++){
                                        for (size_t f = 0; f < X5BlocksTable[5].size(); f++){
                                                uint32_t X5 =
                                                            X5BlocksTable[0][a] << 0 | X5BlocksTable[1][b] << 4 |
                                                            X5BlocksTable[2][c] << 8 | X5BlocksTable[3][d] << 12 |
                                                            X5BlocksTable[4][e] << 16 | X5BlocksTable[5][f] << 20;

                                                uint32_t Y5 = X5;
                                                kuznechik.nonLinearTransform(Y5);
                                                uint32_t C = dCResultsWithAnchor[y].second;
                                                uint32_t K6Prime = C ^ Y5;

                                                vector<uint32_t> δC = vector<uint32_t>();
                                                for (size_t cres = 0; cres < CResults.size(); cres++) {
                                                    if((CResults[cres] ^ C)!=0) {
                                                        δC.push_back(CResults[cres] ^ C);
                                                    }
                                                }
                                                vector<uint32_t> δY5 = δC;
                                                vector<uint32_t> δX5 = vector<uint32_t>(15);
                                                for (size_t index = 0; index < 15; index++) {
                                                   uint32_t dYi = δY5[index];
                                                   uint32_t Yi = Y5;
                                                   uint32_t sum = dYi ^ Yi;
                                                   kuznechik.nonLinearTransformInv(sum);
                                                   kuznechik.nonLinearTransformInv(Yi);
                                                   δX5[index] = sum ^ Yi;
                                                }
                                                
                                                vector<uint8_t> δY4_0 = vector<uint8_t>(15);
                                                for (size_t index = 0; index < 15; index++) {
                                                    kuznechik.linearTransformInv(δX5[index]);
                                                    δY4_0[index] = (uint8_t)δX5[index];
                                                    δY4_0[index]&=0x0F;
                                                }
                                                // Делаем mset(dY4_0)
                                                uint64_t dY4_0Data = 0;
                                                for (size_t index = 0; index < 15; index++) {
                                                    uint64_t dY4_0_64 = δY4_0[index];
                                                    dY4_0_64 = dY4_0_64<<(index*4);
                                                    dY4_0Data |= dY4_0_64;
                                                }
                                                uint32_t mset = Mset::makePair(dY4_0Data);
                                                if (dY4_0Msets.find(mset) !=dY4_0Msets.end()){
                                                    if (K6.count(K6Prime)>0)
                                                    {
                                                        K6[K6Prime]++;
                                                    }
                                                    else {
                                                    
                                                    K6[K6Prime] = 1;
                                                    }
                                                   
                                                }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                
            }
    }
}