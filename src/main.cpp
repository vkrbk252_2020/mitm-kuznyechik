#include <iostream>
#include "Kuznechik.h"
#include "DifferencialWay.h"
#include "Mset.h"
#include <bitset>
#include "MultidimensionalDifferential.h"
#include "KeyTestingDifferencialWay.h"

int main() {

    Kuznechik kuz = Kuznechik();    

    uint32_t lastKey = kuz.roundKeys[kuz.ROUNDS_NUMBER - 1];
    kuz.linearTransformInv(lastKey);

    DifferencialWay diff = DifferencialWay(kuz);
    vector dX3Results = vector<uint32_t>();
    vector X2Results = vector<uint32_t>();
    diff.directDifferential(kuz, dX3Results, X2Results);
    vector dY3Results = vector<uint32_t>();
    vector X4_0Results = vector<uint8_t>();
    diff.reverseDifferential(kuz,dY3Results,X4_0Results);

    vector<Tuple> x2x3x4results = vector<Tuple>();
    diff.checkMiddleValues(kuz,dX3Results,dY3Results,X2Results,X4_0Results,x2x3x4results);
    unordered_map<uint32_t, int> K6 = unordered_map<uint32_t, int>();
    MultidimensionalDifferential multidimensionalDiff = MultidimensionalDifferential();
    unordered_set<uint32_t> dY4_0Msets = multidimensionalDiff.computeDy4_0MSets(kuz,x2x3x4results);

    KeyTestingDifferencialWay keyTest = KeyTestingDifferencialWay(kuz);
    keyTest.findKey(kuz,dY4_0Msets, K6); 

    if (K6.count(lastKey) > 0)
    {
       cout<<"success - "<<K6.find(lastKey)->first<<" "<<K6.find(lastKey)->second<<endl;
    }
    cout<<endl;
    
    cout<<"end"<<endl;
    

    return 0;
}